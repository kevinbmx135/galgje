﻿using Microsoft.VisualBasic;
using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Media.Imaging;
using System.Windows.Threading;

namespace galgje
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        #region declaratie
        int instelTm = 11;
        int levens;
        int Speler = 2;
        int levensVR = 0;
        string vbWoord;
        string geradenLt;
        string ftLetters;
        int seconden;
        string tijd;
        bool hintGevraagd = false;
        string[] tipAlfabeth = new string[]
        {
            "a",
            "b",
            "c",
            "d",
            "e",
            "f",
            "g",
            "h",
            "i",
            "j",
            "k",
            "l",
            "m",
            "n",
            "o",
            "p",
            "q",
            "r",
            "s",
            "t",
            "u",
            "v",
            "w",
            "x",
            "y",
            "z",
        };

        List<string> woord = new List<string>();
        DispatcherTimer timer = new DispatcherTimer();
        string[] woordOpBeeld;
        private string[] galgjeWoorden = new string[]
{
    "grafeem",
    "tjiftjaf",
    "maquette",
    "kitsch",
    "pochet",
    "convocaat",
    "jakkeren",
    "collaps",
    "zuivel",
    "cesium",
    "voyant",
    "spitten",
    "pancake",
    "gietlepel",
    "karwats",
    "dehydreren",
    "viswijf",
    "flater",
    "cretonne",
    "sennhut",
    "tichel",
    "wijten",
    "cadeau",
    "trotyl",
    "chopper",
    "pielen",
    "vigeren",
    "vrijuit",
    "dimorf",
    "kolchoz",
    "janhen",
    "plexus",
    "borium",
    "ontweien",
    "quiche",
    "ijverig",
    "mecenaat",
    "falset",
    "telexen",
    "hieruit",
    "femelaar",
    "cohesie",
    "exogeen",
    "plebejer",
    "opbouw",
    "zodiak",
    "volder",
    "vrezen",
    "convex",
    "verzenden",
    "ijstijd",
    "fetisj",
    "gerekt",
    "necrose",
    "conclaaf",
    "clipper",
    "poppetjes",
    "looikuip",
    "hinten",
    "inbreng",
    "arbitraal",
    "dewijl",
    "kapzaag",
    "welletjes",
    "bissen",
    "catgut",
    "oxymoron",
    "heerschaar",
    "ureter",
    "kijkbuis",
    "dryade",
    "grofweg",
    "laudanum",
    "excitatie",
    "revolte",
    "heugel",
    "geroerd",
    "hierbij",
    "glazig",
    "pussen",
    "liquide",
    "aquarium",
    "formol",
    "kwelder",
    "zwager",
    "vuldop",
    "halfaap",
    "hansop",
    "windvaan",
    "bewogen",
    "vulstuk",
    "efemeer",
    "decisief",
    "omslag",
    "prairie",
    "schuit",
    "weivlies",
    "ontzeggen",
    "schijn",
    "sousafoon"
};
        Dictionary<int, string> dictEindwinnaars = new Dictionary<int, string>();
        #endregion
        public MainWindow()
        {
            InitializeComponent();
        }
        private void MainWindow1_Loaded(object sender, RoutedEventArgs e)
        {
            // Wannneer het window word op gestart dan worden er aantal dingen uitgevoert / toegevoegt aan de variables 
            WprWindow.Background = System.Windows.Media.Brushes.White;
            dictEindwinnaars.Add(1, "Steven – 3 levens (14:10:00)");
            dictEindwinnaars.Add(2, "Yozkan – 7 levens (12:30:51)");
            dictEindwinnaars.Add(3, "Elizabeth – 8 levens (12:31:59)");
            StartGame(); //word de methode start geactiveert waar het spel alles juit aan past he het spel ingesteld staat.

        }
        private void BtnNieuwpel_Click(object sender, RoutedEventArgs e)
        {
            //wanneer het spel opnieuw wilt spelen gaat die detimer stoppen en het woord verwijderen uit het syteem 
            //uitvoer
            timer.Stop();
            woord.Clear();
            StartGame();
        }
        private void BtnVerbergWoord_Click(object sender, RoutedEventArgs e)
        {
            tijd = DateTime.Now.ToLongTimeString(); //zet de live tijd wannner je het spel start ( wanneer het woord moet geraden worden ) en word in een variable gezet dat later word gebruikt voor de highscore .
            if (Speler == 1) // Hier kiest het systeem zelf het woord uit de woorden lijst wat in het systeeem staat geprogrameert voor als je alleen speelt
            {
                int WelkWd;
                Random radom = new Random();
                WelkWd = radom.Next(0, galgjeWoorden.GetLength(0) + 1); // hier word een radom gerbuikt om een random getal te kiezen voor danhet woord te kiezen wat je moet raden.
                vbWoord = galgjeWoorden[WelkWd]; // hier word het woord gekozen in de array van de woorden lijst 
            }
            else
            {
                // hier word het het woord gepakt wat je in getypt als je met twee spelers speelt dus 
                vbWoord = TxtWoord.Text;

            }
            for (int i = 0; i < vbWoord.Length; i++)
            {
                // Hier word het woord op gesplits en dan in een list  gezet voor alle letters van het woord in te zetten voor dan loater te kunne gerbuiken voor de letters te controleren als ze juist geraden zijn 
                woord.Add(vbWoord.Substring(i, 1));
            }

            //uitvoer
            BtnVerbergWoord.Visibility = Visibility.Hidden;
            BtnNieuwpel.Visibility = Visibility.Visible;
            MnuTimer.IsEnabled = false;
            hintGevraagd = false;
            MnuHint.IsEnabled = true;
            BtnRaad.IsEnabled = true;
            TxtWoord.Text = "";

            //timer maken en starten 

            timer.Tick += new EventHandler(DispatcherTimer_Tick);
            timer.Interval = new TimeSpan(0, 0, 1);
            timer.Start();
            //aantal streepyes voor te weten hoelang het woord is 
            woordOpBeeld = new string[woord.Count];
            for (int i = 0; i < woord.Count; i++)
            {
                woordOpBeeld[i] = "*";

            }
            Uitvoer();


        }
        private void BtnRaad_Click(object sender, RoutedEventArgs e)
        {
            Controle(TxtWoord.Text); // gaat naar de methode controle waar ze het woord gaan controleren 
            TxtWoord.Clear();      // maakt het textvak leeg waar je het woord moet in typen 
            seconden = instelTm;  // word de ingevoerde tijd wat al in het begin al is ingesteld op normaal 10 seconden +1 ( voor de seconde pauze bij verandereig in de game ) 


        }
        private void Controle(string geradenWoord) // Hier in deze methode word er gecontroleerd op het woord/letters goed of fout geraden is. 
        {
            int juistGR = 0;
            if (TxtWoord.Text.Length > 1) // controleren als het woord is of een letter is dat ingetypt word 
            {
                if (vbWoord == geradenWoord) // als het woord juist is komt er een te staan dat je gewonnen hebt en word je bij de hihgscore teogevoergt 
                {
                    string tekst = "Hoera!!!";
                    tekst += Environment.NewLine;
                    tekst += $"je hebt ";
                    tekst += Environment.NewLine;
                    tekst += $"'{vbWoord}' ";
                    tekst += Environment.NewLine;
                    tekst += $"correct geraden  ";
                    lblMelding.Content = tekst;
                    //timer word gestopt
                    timer.Stop();
                    // je kan het niet meer opnieuw raden anders is dat een beetje oneerlijk
                    BtnRaad.IsEnabled = false;
                    HighscoreToevoegen();
                }
                else
                {
                    // als je het fout heb geraden dan gaat u leven er af en word het ook te zien in op het scherm en word er ook gecontroleer via methode als u levens op zijn 
                    levens--;
                    levensVR++;
                    LevensOp();
                    Uitvoer();



                }



            }
            if (TxtWoord.Text.Length == 1) // gaat dit doen als er maar 1 letter word in gegeven 
            {
                for (int i = 0; i < woord.Count; i++)
                {
                    if (woord[i] == geradenWoord) // controleren als er 1  van de letters over 1 komt met de letter die raad 
                    {
                        geradenLt += woord[i]; 
                        woordOpBeeld[i] = woord[i];
                        Uitvoer();
                        juistGR = 1; // als er een letter juist dat er dan word door ggeven voor later 
                    }

                }
                if (juistGR == 0) // als de letter fout is dat die dan leven aftrekt en de foute letter op dan laat zijn bij de rets van de foute woorden 
                {
                    levens--;
                    levensVR++;
                    ftLetters += geradenWoord;

                    Uitvoer();
                    LevensOp();
                }
            }
        } 
        private void LevensOp()//controleert als er nog levens zijn 
        {
            // Hier word gecontroleers als de levens op zijn en als de levens op zijn dan stop het spel maar kanje wel nog een neiwu spel starten 
            if (levens == 0)
            {
                // hetspel laat zijn dat je verloren hebt en dat je op gehangen bent op het spel dus verloren hebt 
                string tekst = "je hebt het geheim woord ";
                tekst += Environment.NewLine;
                tekst += $"niet geraden ";
                tekst += Environment.NewLine;
                tekst += $"Je bent opgehangen!  ";
                lblMelding.Content = tekst;
                timer.Stop();
            }

        }
        private void Uitvoer()
        {
            seconden = instelTm;
            string lengtWoord = null;
            for (int i = 0; i < woordOpBeeld.GetLength(0); i++)
            {
                lengtWoord += woordOpBeeld[i]; // voegt de letters en streepjes die nog / geraden zijn geweest of nog geraden moeten worden 
            }
            string tekst; // zer hier de tekst in wat te voor schijn komt in de label wat de speler ziet en kan die alles bij houden hoeveel levens en wat er allemaal al geranden is geweest
            tekst = $"{levens} levens";
            tekst += Environment.NewLine;
            tekst += $"Juiste letters: {geradenLt}";
            tekst += Environment.NewLine;
            tekst += $"Foute Letters :  {ftLetters}";
            tekst += Environment.NewLine;
            tekst += $"  {lengtWoord}";
            lblMelding.Content = tekst;
            TekennenMannetje(); // methode tekent het mannetje aan de hant van hoeveel levens je kwijt gespeelt hebt 
            BtnRaad.IsEnabled = true;
        }
        private void StartGame()
        {
            timer.Stop(); 
            seconden = instelTm; // stelt de timer terug in naar zijn begin waarde 
            tijd = null;
            // reset de levens terug naar 10 leven en de verloren levens terug naar 0 
            levens = 10;
            levensVR = 0;
            // verandert de naam van de knopppen en de tekst aan de hand als je met 1 of twee spelers speelt 
            if (Speler == 1)
            {
                BtnVerbergWoord.Content = "start Spel";
            }
            else
            {
                //variablen 
                string Tekst;

                //berwerking
                Tekst = "Geef een geheim woord in ";

                //uitvoer
                BtnVerbergWoord.Content = "Verberg Woord";
                lblMelding.Content = Tekst;
            }


            //stuk wat zwz moet gebeuren 
            BtnNieuwpel.Visibility = Visibility.Hidden;
            BtnRaad.IsEnabled = false;
            BtnVerbergWoord.Visibility = Visibility.Visible;
            MnuTimer.IsEnabled = true;
            MnuHint.IsEnabled = false;
            lblTimer.Content = "";
            TxtWoord.Text = "";
            TekennenMannetje();



        }
        private void DispatcherTimer_Tick(object sender, EventArgs e)
        {
            // controleert als de timert op nul is en dan geeft die een meldign aan de spelet dat de hij te lang gewacht heeft en verliest die een leven 
            if (seconden == 0)
            {
                levens--;
                levensVR++;
                Uitvoer();
                WprWindow.Background = System.Windows.Media.Brushes.Red; // verandert de achter grond kleu naar rood 
                MessageBox.Show($"je hebt een leven af omfat je te lang wacht je hebt maar{ instelTm} secodnen ");
                seconden = instelTm;
                
            }
             // verandert na dat de achtergrond al naar rood is verandert terug naar white de oorsprongkelijke kleur en teld verder af als de timer niet op nul staat 
            WprWindow.Background = System.Windows.Media.Brushes.White;
            seconden--;
            // laat de tijd op de label zien waat die in moet komen te staan en word constant herladen om de seconden 
            lblTimer.Content = seconden.ToString();

        }
        private void MnuExit_Click(object sender, RoutedEventArgs e)// als je op die knop drukt dan sluit de spel zich af ook zit het spel te spelen 
        {
         
            this.Close();
        }
        private void MnuSingelplayer_Click(object sender, RoutedEventArgs e)// stel je het spel in op  singel player 
        {
            Speler = 1;
            StartGame();

        }
        private void MnuTweeSpelers_Click(object sender, RoutedEventArgs e) // stel je het spel in op twee spelers 
        {
            Speler = 2;
            StartGame();
        }
        private void TekennenMannetje()// tekent het mannetje die op hant voor de speler te laten zien hoeveel levens die nog heeft 
        {
            // zoekt de foot aan de hant heoeveel levens verloren hebt en zit di zo een dan op de canvas wat der na de speler kan zien 
            Uri fotoOphalen = new Uri($"foto's/galgje-{levensVR}.png", UriKind.Relative);
            BitmapImage foto = new BitmapImage(fotoOphalen);
            cnvImgs.Source = foto;
        }
        // staan alle methodes in om high score toetevoegen tot laten te zien 
        #region Hiighscore
        private void LaatHighscoreZien()// gaat de score laten zien in een messagbox die te voor schijun komt 
        {
            string listItem = "";



            foreach (KeyValuePair<int, string> kvp in dictEindwinnaars) //laten zien van de high scores
            {

                listItem += $"{kvp.Value} \n\r";

            }
            MessageBox.Show(listItem);

        }
        private void HighscoreToevoegen() // voegt de een highscore toe die bij de rest van de scores als de speler gewonne heeft 
        {
            // controleert als er geen hint is gevraaagt anders mag die de highscore niet toevoegen in het systeem 
            while (hintGevraagd == false)
            {


                bool verandert = false;
                try
                {
                    string NaamSpeler = Interaction.InputBox("voer hier uw naam in?");
                    // controleert als de levens niet onder nul zijn gegaanen als er wel een naam is in gegeven 
                    if (levensVR > -1 && NaamSpeler.Length > 0 && tijd.Length > 0)
                    {
                        foreach (KeyValuePair<int, string> kvp in dictEindwinnaars)
                        {
                            string[] woorden = kvp.Value.Split(' ');
                            string controleLV = woorden[2]; // zet de levens verloren van andere telkens in een variable voor te later mee het verschil van levens te controleren 
                            //  wordt gecontroleert als de nieuwe highscore minder levens heeft verloren dan de rest en word dan op de plaats gezet als die minder levens heeft verloren 
                            if (Convert.ToInt32(controleLV) > levensVR)
                            {
                                // onthuid de plaats waar de nieuwe highscore moet staan 
                                int plaats = kvp.Key;
                                //maakt een plaats van achter bij voor dan eerts de nieuwe highscore daar te zetten.
                                dictEindwinnaars.Add(dictEindwinnaars.Count + 1, $"{NaamSpeler } - {levensVR } levens ({tijd})");
                                int i = dictEindwinnaars.Count;
                                while (i > plaats)
                                {
                                    //hier worden alle highscores een plaats naar achter gezet zodat de nieuwe op de juiste plaats komt te staan bvp. laats 1
                                    dictEindwinnaars[i] = dictEindwinnaars[i - 1];
                                    i--;
                                }
                                // zet de nieuwe highscore op juiste plaats op het laatse wanneer alle scores verplaats zijn naar de jusite plaats
                                dictEindwinnaars[plaats] = $"{NaamSpeler } - {levensVR } levens ({tijd})";
                                verandert = true;
                                break;
                            }
                            else
                            {
                                verandert = false;
                            }

                        }

                        // controle word gedaan als de nieuwe high score al in het systeem staat en beter was of niet zo niet word die van achter bij gezet als laatste 
                        if (verandert == false)
                        {
                            int NieuwePL = dictEindwinnaars.Count + 1;
                            dictEindwinnaars.Add(NieuwePL, $"{NaamSpeler } - {levensVR } levens ({tijd})");
                        }




                    }
                    else
                    {
                        // geeft een melding als iets mis is geggaan 
                        MessageBox.Show("Er is een probleem opgetreden bij het toevoegen!");

                    }


                }
                catch (Exception)
                {
                    // geeft een melding als iets mis is geggaan 
                    MessageBox.Show("Er is een probleem opgetreden bij het toevoegen!");
                   
                }
                break;
            }
        }
        private void MnuHighScore_Click(object sender, RoutedEventArgs e) // is de knop voor de highscore te zien 
        {

            LaatHighscoreZien();
        }
        #endregion  
        private void MnuTimer_Click(object sender, RoutedEventArgs e)// kan je timer zijn tijd veranderen voor meer of weineger tijd te hebben voor het letter/woord te raden 
        {

            try
            {
                // er komt een inputbox tevoor schijn en daar kun je getal intikken van hoeveel seconden je wilt hebben voor na te denken voor het woord te raden 
                string tijd = Interaction.InputBox("hoelang wil jij voor een letter / woord te raden ");
                instelTm = int.Parse(tijd) + 1;// word een seconden bij geteld voor een seconden spatie hebben als er iets gebeurt in het spel 
            }
            catch (Exception)
            {
                // geeft pas een fout meldign als er een letter word in gegeven / iets fout word ingegeven
                MessageBox.Show(" je hebt de verkeerde waarde door gegeven het moeten cijfers zijn en geen letters!");
            }

        }
        private void MnuHint_Click(object sender, RoutedEventArgs e) // is de knop voor een hint te vragane vooor  een letter die niet klopt 
        {
            List<string> ltFoutGeraden = new List<string>();
            bool letterGevonden = false;
            Random rdmAlfb = new Random();
            string tipLT = "";
            //is een loop voor te controleren al er een letter is gevondden voor de speelr een hint te geven 
            while (letterGevonden == false)
            {
                ltFoutGeraden.Clear();
                int tipLetters = rdmAlfb.Next(0, tipAlfabeth.GetLength(0));
                tipLT = tipAlfabeth[tipLetters];
                int aantal = woord.Count;

                for (int i = 0; i < woord.Count; i++)
                {
                    // word gecontroleert als de letter niet over eeen komt dan met de leters van het woord dat geraden moeten worden
                    if (tipLT != woord[i] && aantal == woord.Count)
                    {
                        letterGevonden = true;
                    }
                    else
                    {
                        aantal--;
                        letterGevonden = false;
                    }

                }
                //conrole gedaan wanneer er een letter gevodnen is geweest
                if (letterGevonden == true && ftLetters != null)
                {
                   // zet de foutive letters in een list 
                    for (int x = 0; x < ftLetters.Length; x++)
                    {
                        ltFoutGeraden.Add(ftLetters.Substring(x,1));
                    }

                    aantal = ltFoutGeraden.Count;
                    for (int y = 0; y < ltFoutGeraden.Count; y++)
                    {
                        //wordt gecontroleerd als de hint letter niet hetzelfde is als de letters die al fout zijn geraden anders moet die een nieuw letter zoeken 
                        string ltfGeraden = ltFoutGeraden[y];
                        if (ltfGeraden == tipLT && aantal == ltFoutGeraden.Count)
                        {
                            letterGevonden = false;
                            break;
                        }
                        
                    }
                }
            }
            //uitvoer wat er hallemaal achteraf gedaan moet worden 
            MessageBox.Show(tipLT);
            levens--;
            levensVR++;
            ftLetters += tipLT;
            hintGevraagd = true; 
            Uitvoer();
            LevensOp();
        }

      
    }
}
